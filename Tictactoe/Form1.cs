﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tictactoe
{
    public partial class TicTacToe : Form
    {
        int turncount = 0;
        bool turn = true;
        Tictac t = new Tictac();

        public TicTacToe()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (turn)
            {
                if (b.Name == "b00") t[0, 0] = 1;
                if (b.Name == "b01") t[0, 1] = 1;
                if (b.Name == "b02") t[0, 2] = 1;
                if (b.Name == "b10") t[1, 0] = 1;
                if (b.Name == "b11") t[1, 1] = 1;
                if (b.Name == "b12") t[1, 2] = 1;
                if (b.Name == "b20") t[2, 0] = 1;
                if (b.Name == "b21") t[2, 1] = 1;
                if (b.Name == "b22") t[2, 2] = 1;
                b.Text = "X";
                label1.Text ="O je na potezu!";

            }
            else
            {
                if (b.Name == "b00") t[0, 0] = 2;
                if (b.Name == "b01") t[0, 1] = 2;
                if (b.Name == "b02") t[0, 2] = 2;
                if (b.Name == "b10") t[1, 0] = 2;
                if (b.Name == "b11") t[1, 1] = 2;
                if (b.Name == "b12") t[1, 2] = 2;
                if (b.Name == "b20") t[2, 0] = 2;
                if (b.Name == "b21") t[2, 1] = 2;
                if (b.Name == "b22") t[2, 2] = 2;
                b.Text = "O";
                label1.Text = "X je na potezu!";

            }

            b.Enabled = false;
            turncount++;

            if (t.checkTic())
            {
                Disable();
                if (turn)
                {
                    label1.Text = "Kraj";
                    MessageBox.Show("X je pobjedio!", "Kraj");
                }
                else
                {
                    label1.Text = "Kraj";
                    MessageBox.Show("O je pobjedio!", "Kraj");
                }
                for (int i = 0; i < 3; i++)
                    for (int j = 0; j < 3; j++)
                        t[i, j] = 0;
            }
            else
            {
                if (turncount == 9)
                {
                    label1.Text = "Kraj";
                    MessageBox.Show("Izjednaceno!", "Kraj");
                }
            }
            turn = !turn;
        }

        private void Disable()
        {

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
                catch { }
            }

            button1.Enabled = true;
            button2.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        { 
            turn = true;
            turncount = 0;
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    t[i, j] = 0;

            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }

                catch { }
            }
            label1.Text = "X je na potezu!";
            button2.Text = "Nova igra";
            button1.Text = "Izlaz";

        }

        private void TicTacToe_Load(object sender, EventArgs e)
        {
            label1.Text = "X je na potezu!";
        }
    }
}
