﻿namespace Tictactoe
{
    partial class TicTacToe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TicTacToe));
            this.b00 = new System.Windows.Forms.Button();
            this.b01 = new System.Windows.Forms.Button();
            this.b02 = new System.Windows.Forms.Button();
            this.b12 = new System.Windows.Forms.Button();
            this.b22 = new System.Windows.Forms.Button();
            this.b11 = new System.Windows.Forms.Button();
            this.b10 = new System.Windows.Forms.Button();
            this.b20 = new System.Windows.Forms.Button();
            this.b21 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // b00
            // 
            resources.ApplyResources(this.b00, "b00");
            this.b00.Name = "b00";
            this.b00.UseVisualStyleBackColor = true;
            this.b00.Click += new System.EventHandler(this.Button_Click);
            // 
            // b01
            // 
            resources.ApplyResources(this.b01, "b01");
            this.b01.Name = "b01";
            this.b01.UseVisualStyleBackColor = true;
            this.b01.Click += new System.EventHandler(this.Button_Click);
            // 
            // b02
            // 
            resources.ApplyResources(this.b02, "b02");
            this.b02.Name = "b02";
            this.b02.UseVisualStyleBackColor = true;
            this.b02.Click += new System.EventHandler(this.Button_Click);
            // 
            // b12
            // 
            resources.ApplyResources(this.b12, "b12");
            this.b12.Name = "b12";
            this.b12.UseVisualStyleBackColor = true;
            this.b12.Click += new System.EventHandler(this.Button_Click);
            // 
            // b22
            // 
            resources.ApplyResources(this.b22, "b22");
            this.b22.Name = "b22";
            this.b22.UseVisualStyleBackColor = true;
            this.b22.Click += new System.EventHandler(this.Button_Click);
            // 
            // b11
            // 
            resources.ApplyResources(this.b11, "b11");
            this.b11.Name = "b11";
            this.b11.UseVisualStyleBackColor = true;
            this.b11.Click += new System.EventHandler(this.Button_Click);
            // 
            // b10
            // 
            resources.ApplyResources(this.b10, "b10");
            this.b10.Name = "b10";
            this.b10.UseVisualStyleBackColor = true;
            this.b10.Click += new System.EventHandler(this.Button_Click);
            // 
            // b20
            // 
            resources.ApplyResources(this.b20, "b20");
            this.b20.Name = "b20";
            this.b20.UseVisualStyleBackColor = true;
            this.b20.Click += new System.EventHandler(this.Button_Click);
            // 
            // b21
            // 
            resources.ApplyResources(this.b21, "b21");
            this.b21.Name = "b21";
            this.b21.UseVisualStyleBackColor = true;
            this.b21.Click += new System.EventHandler(this.Button_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // TicTacToe
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.b21);
            this.Controls.Add(this.b20);
            this.Controls.Add(this.b10);
            this.Controls.Add(this.b11);
            this.Controls.Add(this.b22);
            this.Controls.Add(this.b12);
            this.Controls.Add(this.b02);
            this.Controls.Add(this.b01);
            this.Controls.Add(this.b00);
            this.Name = "TicTacToe";
            this.Load += new System.EventHandler(this.TicTacToe_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b00;
        private System.Windows.Forms.Button b01;
        private System.Windows.Forms.Button b02;
        private System.Windows.Forms.Button b12;
        private System.Windows.Forms.Button b22;
        private System.Windows.Forms.Button b11;
        private System.Windows.Forms.Button b10;
        private System.Windows.Forms.Button b20;
        private System.Windows.Forms.Button b21;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

