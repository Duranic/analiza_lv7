﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tictactoe
{
    class Tictac
    {
        private int[,] M;
        public Tictac()
        {
            M = new int[3, 3] { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
        }
        public int this[int rows, int cols]
        {
            get
            {
                return M[rows, cols];
            }
            set
            {
                M[rows, cols] = value;
            }
        }

        public bool checkTic()
        {
            bool flag = false;
            for (int i = 0; i < 3; i++)
            {
                if (M[i, 0] == M[i, 1] && M[i, 1] == M[i, 2] && M[i, 0] != 0)
                    flag = true;
            }
            for (int i = 0; i < 3; i++)
            {
                if (M[0, i] == M[1, i] && M[1, i] == M[2, i] && M[0, i] != 0)
                    flag = true;
            }
            if (M[0, 0] == M[1, 1] && M[1, 1] == M[2, 2] && M[0, 0] != 0)
                flag = true;
            if (M[0, 2] == M[1, 1] && M[1, 1] == M[2, 0] && M[0, 2] != 0)
                flag = true;
            return flag;
        }

    }
}
